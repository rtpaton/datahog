const api = require("./api/index");
const port = 3001;

api.listen(port, () =>
  console.log(`API server listening at http://localhost:${port}`)
);
