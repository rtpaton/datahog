const express = require("express");
const api = express();

const { validationMiddleware } = require("./middleware");
const { providerQueue } = require("./tasks");

api.post("/datahog", express.json(), validationMiddleware, async (req, res) => {
  res.sendStatus(201);

  const providerTask = {
    sourceUrl: `http://127.0.0.1:3000/providers/${req.body.provider}`,
    callbackUrl: req.body.callbackUrl,
  };

  providerQueue.push(providerTask);
});

module.exports = api;
