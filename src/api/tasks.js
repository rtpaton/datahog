const axios = require("axios").default;

// Use in-memory queues, although could be changed to another storage method for persistance / parallelisation
const callbackQueue = [];
const providerQueue = [];

async function callProvider({ sourceUrl }) {
  const { data } = await axios.get(sourceUrl);
  return data;
}

async function callCallback({ callbackUrl, data }) {
  await axios.post(callbackUrl, data);
}

async function processQueues() {
  if (providerQueue.length) {
    const providerTask = providerQueue.shift();
    try {
      const data = await callProvider(providerTask);
      callbackQueue.push({
        ...providerTask,
        data,
      });
    } catch {
      providerQueue.push(providerTask);
    }
  }

  if (callbackQueue.length) {
    const callbackTask = callbackQueue.shift();
    try {
      await callCallback(callbackTask);
    } catch {
      callbackQueue.push(callbackTask);
    }
  }
}

setInterval(processQueues, 10);

module.exports.providerQueue = providerQueue;
