const Joi = require("joi");

const schema = Joi.object({
  provider: Joi.valid("gas", "internet").required(),
  callbackUrl: Joi.string().uri().required(),
});

module.exports.validationMiddleware = (req, res, next) => {
  const validation = schema.validate(req.body);
  if (validation.error) {
    res.status(400).json({ error: validation.error.details[0].message });
    next(validation.error);
  }
  next();
};
