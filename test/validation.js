const supertest = require("supertest");
const api = supertest(require("../src/api/index"));

describe("Validation", () => {
  it("responds with a 201 if the requests is successful", () => {
    return api
      .post("/datahog")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({
        provider: "gas",
        callbackUrl: "http://some.callback/url",
      })
      .expect(201);
  });

  it("responds with a 400 if neither of the required fields are present in the request", () => {
    return api
      .post("/datahog")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .expect(400);
  });

  it("responds with a 400 if one of the required fields is not present in the request", () => {
    return api
      .post("/datahog")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({
        provider: "gas",
      })
      .expect(400);
  });

  it('responds with a 400 if the provider is not one of "gas" or "internet"', () => {
    return api
      .post("/datahog")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({
        provider: "gass",
        callbackUrl: "http://some.callback/url",
      })
      .expect(400);
  });

  it("responds with a 400 if the callbackUrl does not validate as a URI", () => {
    return api
      .post("/datahog")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({
        provider: "gass",
        callbackUrl: "someurl",
      })
      .expect(400);
  });

  it("returns the first validation error in the response body", () => {
    return api
      .post("/datahog")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({
        provider: "gas",
      })
      .expect({
        error: '"callbackUrl" is required',
      });
  });
});
