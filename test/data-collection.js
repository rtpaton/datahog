const supertest = require("supertest");
const api = supertest(require("../src/api/index"));
const nock = require("nock");
const assert = require("assert");

nock.disableNetConnect();
nock.enableNetConnect("127.0.0.1");

async function tick(milliseconds) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

const gasData = [
  {
    billedOn: "2020-04-07T15:03:14.257Z",
    amount: 22.27,
  },
  {
    billedOn: "2020-05-07T15:03:14.257Z",
    amount: 30.0,
  },
];

afterEach(() => {
  nock.abortPendingRequests();
  nock.cleanAll();
});

describe("Data Collection", () => {
  it("calls the provided provider", async () => {
    const providerScope = nock("http://127.0.0.1:3000")
      .get("/providers/gas")
      .reply(200, gasData);

    nock("http://some.callback").post("/url", gasData).reply(200);

    await api
      .post("/datahog")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({
        provider: "gas",
        callbackUrl: "http://some.callback/url",
      });

    await tick(100);

    assert(providerScope.isDone());
  });

  it("calls the provided provider", async () => {
    const providerScope = nock("http://127.0.0.1:3000")
      .get("/providers/gas")
      .reply(200, gasData);

    nock("http://some.callback").post("/url", gasData).reply(200);

    await api
      .post("/datahog")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({
        provider: "gas",
        callbackUrl: "http://some.callback/url",
      });

    await tick(200);

    assert(providerScope.isDone());
  });

  it("calls the callback with the result", async () => {
    nock("http://127.0.0.1:3000").get("/providers/gas").reply(200, gasData);

    const callbackScope = nock("http://some.callback")
      .post("/url", gasData)
      .reply(200);

    await api
      .post("/datahog")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({
        provider: "gas",
        callbackUrl: "http://some.callback/url",
      });

    await tick(15);

    assert(callbackScope.isDone());
  });

  describe("with failures", () => {
    it("does not return the error to the client", async () => {
      nock("http://127.0.0.1:3000").get("/providers/gas").reply(500);
      nock("http://some.callback").post("/url", gasData).reply(200);

      await api
        .post("/datahog")
        .set("Content-Type", "application/json")
        .set("Accept", "application/json")
        .send({
          provider: "gas",
          callbackUrl: "http://some.callback/url",
        })
        .expect(201);
    });

    it("retries calls to the provider if they fail", async () => {
      const providerScope = nock("http://127.0.0.1:3000")
        .get("/providers/gas")
        .reply(500)
        .get("/providers/gas")
        .reply(500)
        .get("/providers/gas")
        .reply(200, gasData);

      const callbackScope = nock("http://some.callback")
        .post("/url", gasData)
        .reply(200);

      await api
        .post("/datahog")
        .set("Content-Type", "application/json")
        .set("Accept", "application/json")
        .send({
          provider: "gas",
          callbackUrl: "http://some.callback/url",
        });

      await tick(55);

      assert(providerScope.isDone());
      assert(callbackScope.isDone());
    });

    it("retries calls to the callbackUrl if they fail", async () => {
      const providerScope = nock("http://127.0.0.1:3000")
        .get("/providers/gas")
        .reply(500)
        .get("/providers/gas")
        .reply(200, gasData);

      const callbackScope = nock("http://some.callback")
        .post("/url", gasData)
        .reply(500)
        .post("/url", gasData)
        .reply(500)
        .post("/url", gasData)
        .reply(200);

      await api
        .post("/datahog")
        .set("Content-Type", "application/json")
        .set("Accept", "application/json")
        .send({
          provider: "gas",
          callbackUrl: "http://some.callback/url",
        });

      await tick(65);

      assert(providerScope.isDone());
      assert(callbackScope.isDone());
    });
  });
});
